// Copyright Epic Games, Inc. All Rights Reserved.

#include "TEST_PROJECT.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TEST_PROJECT, "TEST_PROJECT" );
