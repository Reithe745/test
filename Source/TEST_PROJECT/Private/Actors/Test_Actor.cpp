// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Test_Actor.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ATest_Actor::ATest_Actor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	RootComponent = MeshComp;

}

// Called when the game starts or when spawned
void ATest_Actor::BeginPlay()
{
	Super::BeginPlay();
	
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TEXT("Ola mundo!"));
	
	LifeAmmount = 10;

	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, FString::FromInt(LifeAmmount));
}

// Called every frame
void ATest_Actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

