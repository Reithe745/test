// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Test_Actor.generated.h"

UCLASS()
class TEST_PROJECT_API ATest_Actor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATest_Actor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Basics")
	int LifeAmmount = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Basics")
	bool Alive = true;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* MeshComp;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
